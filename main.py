#!/usr/bin/env python3

import argparse
import json

import pandas

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-v", action="store_true")
    args = parser.parse_args()
    v = args.v

    # additional member system

    num_seats = 129

    total_seats = pandas.Series(dtype="Float64")

    # first vote: constituency, by first past the post
    # 73 constituencies => 73 seats

    constituency_votes = pandas.read_csv("constituency.csv", index_col=0)

    # normalise names
    constituency_votes.index = constituency_votes.index.str.lower()
    constituency_votes.columns = constituency_votes.columns.str.lower()

    constituency_seats = constituency_votes.idxmax(axis=1)

    if v:
        print("# constituency results\n")

        constituency_perc = constituency_votes.max(axis=1) / constituency_votes.sum(
            axis=1
        )

        constituency_results = pandas.concat(
            [constituency_seats, constituency_perc], axis=1
        )

        print(f"{'constituency':<38} % of votes    winner")
        for constituency, party, percentage in constituency_results.itertuples():
            print(f"{constituency:<45} {percentage:.0%}    {party}")

        print("\n# regions\n")

    # second vote: regional, by D'Hondt initialised with first vote seat results per region
    # 8 regions with 7 seats each => 56 seats

    with open("constituencies_by_region.json") as f:
        constituencies_by_region = json.load(f)

    constituencies_by_region = {
        k: pandas.Series(v) for k, v in constituencies_by_region.items()
    }

    regions_votes = pandas.read_csv("region.csv", index_col=0)
    regions_votes.index = regions_votes.index.str.lower()  # normalise names
    regions = regions_votes.columns

    for region_name in regions:
        region_regional_votes = regions_votes[region_name]

        region_constituencies = constituencies_by_region[region_name].str.lower()
        region_constituency_seats = constituency_seats[
            region_constituencies
        ].value_counts()
        region_seats = region_constituency_seats.copy()

        for _ in range(7):
            quotients = region_regional_votes.div(region_seats + 1, fill_value=1)

            round_winner = quotients.idxmax()

            region_seats[round_winner] = (
                region_seats[round_winner] + 1 if round_winner in region_seats else 1
            )

            quotients[round_winner] = region_regional_votes[round_winner] / (
                region_seats[round_winner] + 1
            )

        total_seats = total_seats.add(region_seats, fill_value=0)

        if v:
            print(f"## {region_name}\n")

            print("### regional (only) results")

            region_regional_n = region_regional_votes.sum()

            region_regional_seats = region_seats.sub(
                region_constituency_seats, fill_value=0
            )

            print(f"{'party':<48} seats")
            for party, seats in (
                region_regional_seats[region_regional_seats > 0]
                .sort_values(ascending=False)
                .items()
            ):
                print(f"{party:<50} {seats:3.0f}")

            print()

            print("### region total results\n")

            region_constituencies_votes = constituency_votes.filter(
                items=region_constituencies, axis=0
            ).sum()

            region_constituencies_n = region_constituencies_votes.sum()

            region_n = region_regional_n + region_constituencies_n

            region_votes = region_regional_votes.add(
                region_constituencies_votes, fill_value=0
            )

            region_results = pandas.concat(
                [region_seats, region_votes],
                axis=1,
            ).fillna(0)
            region_results.columns = ["seats", "votes"]

            print(f"{'party':<48} seats    % of combined votes")
            for party, seats, votes in (
                region_results[region_results["votes"] > 0]
                .sort_values(by=["seats", "votes"], ascending=False)
                .itertuples()
            ):
                print(f"{party:<50} {seats:3.0f}    {votes/region_n:.2%}")

            print()

    # final tally

    if v:
        print("\n# national result\n")

    print(f"{'party':<41} seats    % of seats")
    for party, seats in total_seats.sort_values(ascending=False).items():
        print(f"{party:<43} {seats:3.0f}    {seats/num_seats:3.0%}")
